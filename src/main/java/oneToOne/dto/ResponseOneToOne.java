package oneToOne.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import oneToOne.model.Status;

import java.util.Date;

@Getter
@Builder
@Setter
public class ResponseOneToOne {
    private Long id;

    private Date dateTime;

    private Long creatorId;

    private Long ownerId;

    private String comment;

    private Status status;

    private String linkToInterview;

}
