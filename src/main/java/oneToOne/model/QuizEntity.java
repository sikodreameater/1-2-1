package oneToOne.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name = "quiz")
@NoArgsConstructor
@EqualsAndHashCode
public class QuizEntity {

    static {
//        String  questions = [{
//            "Local variables are defined in the method and scope of the variables that exist inside the method itself.\n" +
//                    "Instance variable is defined inside the class and outside the method and the scope of the variables exists throughout the class.",
//                    "What is a Class?\n" +
//        }, {
//                "\n" +
//                "Answer: All Java codes are defined in a Class. It has variables and methods.\n" +
//                "\n" +
//                "Variables are attributes which define the state of a class.\n" +
//                "\n" +
//                "Methods are the place where the exact business logic has to be done. It contains a set of statements (or) instructions to satisfy the particular requirement."}];
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "one_to_one_meet_id")
    private Long oneToOneMeetId;

    @Column(name = "question")
    private String question;

    @Column(name = "answer")
    private String answer;


}
