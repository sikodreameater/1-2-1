package oneToOne.model;

public class QuestionsDto {
    String quest1 = "What is a Class?\n" +
            "\n" +
            "Answer: All Java codes are defined in a Class. It has variables and methods.\n" +
            "\n" +
            "Variables are attributes which define the state of a class.\n" +
            "\n" +
            "Methods are the place where the exact business logic has to be done. It contains a set of statements (or) instructions to satisfy the particular requirement.";

    String ques2 = "Q #8) What is an Object?\n" +
            "\n" +
            "Answer: An instance of a class is called an object. The object has state and behavior.\n" +
            "\n" +
            "Whenever the JVM reads the “new()” keyword then it will create an instance of that class.\n" +
            "\n";

    String quest3 = "What are the OOPs concepts?\n" +
            "\n" +
            "Answer: OOPs concepts include:\n" +
            "\n" +
            "Inheritance\n" +
            "Encapsulation\n" +
            "Polymorphism\n" +
            "Abstraction\n" +
            "Interface";

    String ques5 = " What is Inheritance?\n" +
            "\n" +
            "Answer: Inheritance means one class can extend to another class. So that the codes can be reused from one class to another class. The existing class is known as the Super class whereas the derived class is known as a sub class.\n" +
            "\n";

    String quest6 = "What is Encapsulation?\n" +
            "\n" +
            "Answer: Purpose of Encapsulation:\n" +
            "\n" +
            "Protects the code from others.\n" +
            "Code maintainability.";

    String ques7 = "What is Polymorphism?\n" +
            "\n" +
            "Answer: Polymorphism means many forms.\n" +
            "\n" +
            "A single object can refer to the super-class or sub-class depending on the reference type which is called polymorphism.";

    String ques8 = "What is meant by Method Overriding?\n" +
            "\n" +
            "Answer: Method overriding happens if the sub-class method satisfies the below conditions with the Super-class method:\n" +
            "\n" +
            "Method name should be the same\n" +
            "The argument should be the same\n" +
            "Return type should also be the same\n" +
            "The key benefit of overriding is that the Sub-class can provide some specific information about that sub-class type than the super-class";

    //Made by Azat Mametov
}
